import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { LoginPage } from '../login/login.page';
import { AuthService } from 'src/app/services/auth.service';
import { NgForm } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  public states: any[];
  public districts: any[];
  public cities: any[];
  public areas:any[];

  public selectedDistricts: any[];
  public selectedCities: any[];
  public selectedAreas:any[];

  public sState: any;
  public sDistrict: any;

  constructor(private modalController: ModalController,
    private authService: AuthService,
    private navCtrl: NavController,
    private alertService: AlertService) { 
      this.initializeState();
        this.initializeDistrict();
        this.initializeCity();
        this.initializeArea();
    }

  ngOnInit() {
  }

  initializeState(){
    this.states = [
        {id: 1, name: 'Andhra Pradesh'},
        {id: 2, name: 'Arunachal Pradesh'},
        {id: 3, name: 'Assam'},
        {id: 4, name: 'Bihar'},
        {id: 5, name: 'Chhattisgarh'},
        {id: 6, name: 'Goa'},
        {id: 7, name: 'Gujarat'},
        {id: 8, name: 'Haryana'},
        {id: 9, name: 'Himachal Pradesh'},
        {id: 10, name: 'Jammu and Kashmir'},
        {id: 11, name: 'Jharkhand'},
        {id: 12, name: 'Karnataka'},
        {id: 13, name: 'Kerala'},
        {id: 14, name: 'Madhya Pradesh'},
        {id: 15, name: 'Maharashtra'},
        {id: 16, name: 'Manipur'},
        {id: 17, name: 'Meghalaya'},
        {id: 18, name: 'Mizoram'},
        {id: 19, name: 'Nagaland'},
        {id: 20, name: 'Odisha'},
        {id: 21, name: 'Punjab'},
        {id: 22, name: 'Rajasthan'},
        {id: 23, name: 'Sikkim'},
        {id: 24, name: 'Tamil Nadu'},
        {id: 25, name: 'Telangana'},
        {id: 26, name: 'Tripura'},
        {id: 27, name: 'Uttar Pradesh'},
        {id: 28, name: 'Uttarakhand'},
        {id: 29, name: 'West Bengal'}


    ];
    }

    initializeDistrict(){
    this.districts = [
        {id: 1, name: 'Adilabad', state_id: 25, state_name: 'Telangana'},
        {id: 2, name: 'Bhadradri Kothagudem', state_id: 25, state_name: 'Telangana'},
        {id: 3, name: 'Hyderabad', state_id: 25, state_name: 'Telangana'},
        {id: 4, name: 'Jagitial', state_id: 25, state_name: 'Telangana'},
        {id: 5, name: 'Jangaon', state_id: 25, state_name: 'Telangana'},
        {id: 7, name: 'Jayashankar Bhupalapally', state_id: 25, state_name: 'Telangana'}
    ];
    }

    initializeCity(){
    this.cities = [
        {id: 1, name: 'Hyderabad', state_id: 25, district_id: 1},
        {id: 2, name: 'Secunderabad', state_id: 25, district_id: 1},
        {id: 3, name: 'City of Jasin 1', state_id: 25, district_id: 2},
        {id: 4, name: 'City of Muar 1', state_id: 25, district_id: 3},
        {id: 5, name: 'City of Muar 2', state_id: 25, district_id: 3},
        {id: 6, name: 'City of Segamat 1', state_id: 25, district_id: 4},
        {id: 7, name: 'City of Shah Alam 1', state_id: 25, district_id: 5},
        {id: 8, name: 'City of Klang 1', state_id: 25, district_id: 6},
        {id: 9, name: 'City of Klang 2', state_id: 25, district_id: 6}
    ];
    }
    initializeArea(){
      this.areas = [
        {id: 1, name: 'Narsingi', state_id: 25, city_id: 1},
        {id: 2, name: 'Secunderabad', state_id: 25, city_id: 1},
        {id: 3, name: 'Area of Jasin 1', state_id: 25, city_id: 1},
        {id: 4, name: 'Area of Muar 1', state_id: 25, city_id: 1},
        {id: 5, name: 'Area of Muar 2', state_id: 25, city_id: 1},
        {id: 6, name: 'Area of Segamat 1', state_id: 25, city_id: 2},
        {id: 7, name: 'Area of Shah Alam 1', state_id: 25, city_id: 2},
        {id: 8, name: 'Area of Klang 1', state_id: 25, city_id: 3},
        {id: 9, name: 'Area of Klang 2', state_id: 25, city_id: 4}
    ];
      
    }
    
    setDistrictValues(sState) {
      console.log(sState);
      this.selectedDistricts = this.districts.filter(district => district.state_id == sState.detail.value.id)
  }

  setCityValues(sDistrict) {
      this.selectedCities = this.cities.filter(city => city.district_id == sDistrict.detail.value.id);
  }

  setAreaValues(sCity){
    console.log(sCity);
    this.selectedAreas = this.areas.filter(area => area.city_id == sCity.detail.value.id);
  }

  // Dismiss Register Modal
  dismissRegister() {
    this.modalController.dismiss();
  }
  // On Login button tap, dismiss Register modal and open login Modal
  async loginModal() {
    this.dismissRegister();
    const loginModal = await this.modalController.create({
      component: LoginPage,
    });
    return await loginModal.present();
  }
  register(form: NgForm) {
    this.authService.register(form.value.fName, form.value.lName, form.value.email, form.value.password).subscribe(
      data => {
        this.authService.login(form.value.email, form.value.password).subscribe(
          data => {
          },
          error => {
            console.log(error);
          },
          () => {
            this.dismissRegister();
            this.navCtrl.navigateRoot('/dashboard');
          }
        );
        this.alertService.presentToast(data['message']);
      },
      error => {
        console.log(error);
      },
      () => {
        
      }
    );
  }

}
