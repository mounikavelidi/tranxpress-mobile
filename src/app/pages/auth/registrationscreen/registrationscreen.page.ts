import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registrationscreen',
  templateUrl: './registrationscreen.page.html',
  styleUrls: ['./registrationscreen.page.scss'],
})
export class RegistrationscreenPage implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }
  RegisterOwner(){
    this.router.navigate(['/register'])
  }
  RegisterVendor(){
    this.router.navigate(['/register'])
  }
}
