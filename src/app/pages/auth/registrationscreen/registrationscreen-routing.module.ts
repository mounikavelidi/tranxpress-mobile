import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistrationscreenPage } from './registrationscreen.page';

const routes: Routes = [
  {
    path: '',
    component: RegistrationscreenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegistrationscreenPageRoutingModule {}
