import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegistrationscreenPageRoutingModule } from './registrationscreen-routing.module';

import { RegistrationscreenPage } from './registrationscreen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegistrationscreenPageRoutingModule
  ],
  declarations: [RegistrationscreenPage]
})
export class RegistrationscreenPageModule {}
