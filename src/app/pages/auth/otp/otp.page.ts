import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-otp',
  templateUrl: './otp.page.html',
  styleUrls: ['./otp.page.scss'],
})
export class OtpPage implements OnInit {
  otpForm: FormGroup;
  constructor(public formBuilder: FormBuilder,private router:Router) { }

  ngOnInit() {
    this.setupForm()
  }
  setupForm() {
    this.otpForm = this.formBuilder.group({
      otp1: ['', [Validators.maxLength(1), Validators.required]],
      otp2: ['', [Validators.maxLength(1), Validators.required]],
      otp3: ['', [Validators.maxLength(1), Validators.required]],
      otp4: ['', [Validators.maxLength(1), Validators.required]],
      otp5: ['', [Validators.maxLength(1), Validators.required]],
      otp6: ['', [Validators.maxLength(1), Validators.required]],
    });
  }
  moveFocus(event, next, prev) {
    if (event.target.value.length < 1 && prev) {
      prev.setFocus()
    }
    else if (next && event.target.value.length > 0) {
      next.setFocus();
    }
    else {
      return 0;
    }
  }
  onSubmit(){
    this.router.navigate(['/registrationscreen'])
  }
}
